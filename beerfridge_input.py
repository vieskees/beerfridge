import time
from datetime import datetime
from firebase import firebase
import firebase_admin
from firebase_admin import credentials
from pytz import timezone

import get_temperature
import get_brewery_status
import get_fridge_status
import set_temp

cred = credentials.Certificate("baestert-brewery-firebase-adminsdk-ud9wd-bbc2d33e94.json")
firebase_admin.initialize_app(cred)

firebase = firebase.FirebaseApplication('https://baestert-brewery.firebaseio.com/thing', None)

temp_inside_sensor = '/sys/bus/w1/devices/28-0516a15087ff/w1_slave'
temp_outside_sensor = '/sys/bus/w1/devices/28-0516a15fefff/w1_slave'

read_interval = 60

while True:
        try:
                time_datetime = datetime.now(timezone('Europe/Amsterdam'))
                time_now = time_datetime.strftime('%d-%m-%Y %H:%M:%S')
                temperature_inside = '{0:01.2f}'.format(get_temperature.read_temp(temp_inside_sensor))
                temperature_outside = '{0:01.2f}'.format(get_temperature.read_temp(temp_outside_sensor))
                temperature_desired = set_temp.get_set_temperature()
                cooling_status = get_fridge_status.is_cooling()
                brewing = get_brewery_status.brewery_running()

                brewStatusJson = {
                        'time':time_now,
                        'temp_wort':temperature_inside,
                        'temp_fridge':temperature_outside,
                        'temp_goal': temperature_desired,
                        'is_cooling': cooling_status,
                        'is_brewing':brewing
                }
                result = firebase.post('/thing',brewStatusJson)
                print 'Record inserted'
                time.sleep(read_interval)
        except IOError:
                print('Error! Something went wrong.')
                time.sleep(read_interval)
