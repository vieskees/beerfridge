#!/usr/bin/env python

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, GPIO.PUD_UP)

def brewery_running():
	if GPIO.input(18):
		return False
	else:
		return True

