import time
import re
def temp_raw(sensor):
	with open(sensor, 'r') as f:
		lines = f.readlines()

	return lines

def read_temp(sensor):
	lines = temp_raw(sensor)
	while 'YES' not in lines[0]:
		time.sleep(0.2)
		lines = temp_raw(sensor)

	m = re.match('.*t=(\d+)', lines[1])

	if m:
		temp = m.group(1)
		return float(temp) / 1000.0