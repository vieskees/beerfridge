import socket
import json
from struct import pack

ip = "192.168.1.80"
port = 9999
cmd = '{"system":{"get_sysinfo":{}}}'


def encrypt(string):
        key = 171
        result = pack('>I', len(string))
        for i in string:
                a = key ^ ord(i)
                key = a
                result += chr(a)
        return result

def decrypt(string):
        key = 171
        result = ""
        for i in string:
                a = key ^ ord(i)
                key = ord(i)
                result += chr(a)
        return result

def decrypt(string):
        key = 171
        result = ""
        for i in string:
                a = key ^ ord(i)
                key = ord(i)
                result += chr(a)
        return result

def running():
        sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock_tcp.connect((ip, port))
        sock_tcp.send(encrypt(cmd))
        data = sock_tcp.recv(2048)
        sock_tcp.close()

        jsonData = decrypt(data[4:])
        jsonToPython = json.loads(jsonData)
        state = str(jsonToPython['system']['get_sysinfo']['relay_state'])
        return state

def is_cooling():
        if running()=="1":
                return True
        else:
                return False