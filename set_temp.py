import Adafruit_MCP3008

CLK  = 16
MISO = 23
MOSI = 24
CS   = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)



def get_set_temperature():
 value = mcp.read_adc(0)
 value = round((value/(1024/15.0)+10)*2)/2
 return value