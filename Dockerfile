# Python Base Image from https://hub.docker.com/r/arm32v7/python/
FROM arm32v7/python:2.7.13-jessie

COPY get_temperature.py ./get_temperature.py
COPY get_fridge_status.py ./get_fridge_status.py
COPY get_brewery_status.py ./get_brewery_status.py
COPY beerfridge_input.py ./beerfridge_input.py
COPY set_temp.py ./set_temp.py
COPY baestert-brewery-firebase-adminsdk-ud9wd-bbc2d33e94.json ./baestert-brewery-firebase-adminsdk-ud9wd-bbc2d33e94.json

# Intall the necessary modules
RUN pip install --no-cache-dir rpi.gpio
RUN pip install python-firebase
RUN pip install firebase-admin
RUN pip install adafruit-mcp3008
RUN pip install Adafruit-LED-Backpack

CMD python /beerfridge_input.py && \
    python /set_temp.py