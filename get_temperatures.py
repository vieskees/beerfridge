import time
import re
def temp_raw(sensor):
	with open(sensor, 'r') as f:
		lines = f.readlines()

	return lines

def read_temp(sensor):
	lines = temp_raw(sensor)
	while 'YES' not in lines[0]:
		time.sleep(0.2)
		lines = temp_raw(sensor)

	m = re.match('.*t=(\d+)', lines[1])

	if m:
		temp = m.group(1)
		return float(temp) / 1000.0

while True:
	sensor = '/sys/bus/w1/devices/28-0516a15087ff/w1_slave'
	sensor2 = '/sys/bus/w1/devices/28-0516a15fefff/w1_slave'
	print("binnen")
	print('{0:0.1f}'.format(read_temp(sensor)))
	print("buiten")
	print('{0:0.1f}'.format(read_temp(sensor2)))
	time.sleep(1)