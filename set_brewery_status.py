#!/usr/bin/env python

import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(5, GPIO.OUT)

GPIO.output(5, GPIO.HIGH)

def callback_func(pin):
	if GPIO.input(18):
		GPIO.output(5, False)
		print "uit"
	else:
		GPIO.output(5, True)
		print ("aan")

GPIO.add_event_detect(18, GPIO.BOTH, callback=callback_func, bouncetime=200)

def main():
	while True:
		time.sleep(1)

if __name__ == "__main__":
	main()